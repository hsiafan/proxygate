#!/bin/sh

cd fe && npm run build
cd ..

rm -fv fe/dist/js/*.map

export GOPATH=`readlink -f ../../`
go-bindata fe/dist/...
mkdir -p deploy && go build -i -o deploy/proxygate proxygate
