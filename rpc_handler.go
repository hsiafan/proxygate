package main

import (
	"strconv"
	"net/http"
	"reflect"
	"strings"
	"errors"
	"encoding/json"
)

// ControllerToHandlers wrap all pub methods in controller as http handler
func ControllerToHandlers(c interface{}) []*APIHandler {
	var value = reflect.ValueOf(c)
	var t = value.Type()
	if t.Kind() != reflect.Ptr {
		// should use reflect value
		logger.Error("not a struct pointer")
		return nil
	}

	var controllerName = t.Elem().Name()
	controllerName = strings.TrimSuffix(controllerName, "Controller")
	controllerName = strings.ToLower(controllerName[0:1]) + controllerName[1:]
	var methodNum = t.NumMethod()
	var handlers []*APIHandler
	for i := 0; i < methodNum; i++ {
		var m = t.Method(i)
		var handler, err = NewAPIHandler(value, m)
		if err != nil {
			logger.Warn("get handler from method {} error: {}", m.Name, err)
			continue
		}
		handler.Path = "/" + controllerName + "/" + handler.Path
		handlers = append(handlers, handler)
	}
	return handlers
}

// APIResponse json api response
type APIResponse struct {
	Code    int         `json:"code"`
	Message string      `json:"msg"`
	Data    interface{} `json:"data"`
}

// APIHandler create api from go func
type APIHandler struct {
	Method  string
	Path    string
	Handler func(http.ResponseWriter, *http.Request)
}

// NewAPIHandler create new api handler from a controller struct
func NewAPIHandler(receiver reflect.Value, method reflect.Method) (*APIHandler, error) {

	var name = method.Name
	var t = method.Type
	if t.NumIn() > 2 {
		return nil, errors.New("method " + name + " has more than one parameter, count:" + strconv.Itoa(t.NumIn()))
	}
	if t.NumOut() < 1 || t.NumOut() > 2 {
		return nil, errors.New("method " + name + " should return 1 or 2 values but " + strconv.Itoa(t.NumOut()))
	}

	var errorType = reflect.TypeOf((*error)(nil)).Elem()
	var lastReturnType = t.Out(t.NumOut() - 1)
	if !lastReturnType.Implements(errorType) {
		return nil, errors.New("method " + name + " should return error as last value but " + lastReturnType.String())
	}

	var handler = func(w http.ResponseWriter, r *http.Request) {
		var params = []reflect.Value{receiver}
		if t.NumIn() == 2 {
			var paramType = t.In(1)
			var param reflect.Value
			if paramType.Kind() == reflect.Ptr {
				param = reflect.New(paramType.Elem())
			} else {
				param = reflect.New(paramType)
			}
			if !param.IsValid() {
				logger.Error("get param failed, param is not valid")
				return
			}
			var paramIf = param.Interface()
			if err := json.NewDecoder(r.Body).Decode(&paramIf); err != nil {
				json.NewEncoder(w).Encode(&APIResponse{
					Code:    -2,
					Message: err.Error(),
				})
				return
			}
			if paramType.Kind() != reflect.Ptr {
				param = reflect.Indirect(param)
			}
			logger.Debug("deserialize rpc params:{}", param)
			params = append(params, param)
		}

		var returnValues = method.Func.Call(params)
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")

		var err = returnValues[t.NumOut()-1]
		if !err.IsNil() {
			json.NewEncoder(w).Encode(&APIResponse{
				Code:    -1,
				Message: err.Interface().(error).Error(),
			})
			return
		}

		var data interface{}
		if t.NumOut() == 2 {
			data = returnValues[0].Interface()
		}
		json.NewEncoder(w).Encode(&APIResponse{
			Code: 0,
			Data: data,
		})
	}

	return &APIHandler{Path: strings.ToLower(name[0:1]) + name[1:], Handler: handler}, nil
}
