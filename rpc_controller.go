package main

import (
	"proxygate/base"
)

// LocalController api for local proxy setting
type LocalController struct {
}

// NewLocalController return new LocalController
func NewLocalController() *LocalController {
	return &LocalController{}
}

// GetLocalProxy return local config
func (lc *LocalController) GetLocalProxy() (*base.LocalSetting, error) {
	return server.LocalSetting(), nil
}

// SaveLocalProxy save local config
func (lc *LocalController) SaveLocalProxy(setting *base.LocalSetting) error {
	if err := server.SetLocalSetting(setting); err != nil {
		return err
	}
	server.Stop()
	if err := server.Start(); err != nil {
		logger.Critical("start server error: {}", err)
		return err
	}
	return nil
}

// ProxyController api for proxy server settings
type ProxyController struct {
}

// NewProxyController create new ProxyController
func NewProxyController() *ProxyController {
	return &ProxyController{}
}

// GetProxies return all proxies
func (pc *ProxyController) GetProxies() ([]*base.SecondaryProxy, error) {
	return server.Proxies(), nil
}

// AddProxy add proxy server
func (pc *ProxyController) AddProxy(proxy *base.SecondaryProxy) error {
	var _, err = server.AddProxy(proxy)
	return err
}

// UpdateProxy update proxy server
func (pc *ProxyController) UpdateProxy(proxy *base.SecondaryProxy) error {
	return server.UpdateProxy(proxy)
}

// DeleteProxy delete proxy server
func (pc *ProxyController) DeleteProxy(id int) error {
	return server.DeleteProxy(id)
}
