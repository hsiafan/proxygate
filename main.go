package main

import (
	"github.com/clearthesky/vlog"
	"proxygate/base"
	"net/http"
	"github.com/elazarl/go-bindata-assetfs"
)

var logger = vlog.CurrentPackageLogger()

var server *base.Server

func main() {
	var err error
	server, err = base.NewServer()
	if err != nil {
		logger.Critical("create server error: {}", err)
		return
	}

	var mux = http.NewServeMux()
	registerControllers(mux, NewLocalController(), NewProxyController())

	//static files
	var staticFS = &assetfs.AssetFS{Asset: Asset, AssetDir: AssetDir, AssetInfo: AssetInfo, Prefix: "fe/dist"}
	mux.Handle("/", http.FileServer(staticFS))
	server.HttpHandler = mux

	if err := server.Start(); err != nil {
		logger.Critical("start server error: {}", err)
		return
	}
	select {}
}

func registerControllers(mux *http.ServeMux, controllers ...interface{}) {
	for _, c := range controllers {
		for _, h := range ControllerToHandlers(c) {
			logger.Debug("add handler at path: {}", h.Path)
			mux.HandleFunc("/api"+h.Path, h.Handler)
		}
	}
}
