package main

import (
	"strings"
)

// SubStrBefore return sub string before sep
func subStrBefore(str string, sep string) string {
	idx := strings.Index(str, sep)
	if idx < 0 {
		return str
	}
	return str[0:idx]
}
