// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'
import App from './App'

Vue.config.productionTip = false

Vue.use(VueResource)
Vue.use(ElementUI)

const MyPlugin = {}
MyPlugin.install = function (Vue, options) {

  Vue.prototype.$getRpcController = function (name) {
    let http = this.$http;
    let handler = {
      get(target, propKey, receiver) {
        return function (...args) {
          let path = "/api/" + target["name"] + "/" + propKey;
          if (args.length > 1) {
            return Promise.reject(new Error("too many parameters"));
          }
          let promise =
            args.length == 0 ? http.post(path) : http.post(path, args[0]);
          return promise.then(response => {
            let body = response.body;
            if (body.code == 0) {
              return body.data;
            }
            throw new Error(body.msg);
          });
        };
      }
    };
    return new Proxy({ name: name }, handler);
  };
  Vue.prototype.$showError = function (msg) {
    this.$message({
      duration: 0,
      showClose: true,
      message: msg,
      type: "error"
    });
  }

}
Vue.use(MyPlugin)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
