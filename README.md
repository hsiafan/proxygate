Proxy Gate

A Proxy, with gui/webui, adapt shadowsocks/socks5/http backends, to both socks5/http proxy, using the same port.

![flow chart](https://raw.githubusercontent.com/clearthesky/proxygate/master/images/proxygate.png)
