package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"strconv"
	"reflect"
)

func TestNewAPIHandler(t *testing.T) {
	var test = &Test{}
	var m, _ = reflect.TypeOf(test).MethodByName("GetName")
	var handler, err = NewAPIHandler(reflect.ValueOf(test), m)
	assert.NoError(t, err)
	assert.NotNil(t, handler)
}

type Test struct {
}

func (t *Test) GetName(v int) (string, error) {
	return "123" + strconv.Itoa(v), nil
}

func (t *Test) name(v int) (string, error) {
	return "", nil
}

func TestControllerToHandlers(t *testing.T) {
	var test = &Test{}
	var handlers = ControllerToHandlers(test)
	assert.NotEmpty(t, 1, len(handlers))
}
