package base

import (
	"net"
	"net/http"
	"encoding/json"
)

// HTTPServer handle plain http request.
// This server handle pac request, and delegate other request to handler set
type HTTPServer struct {
	listener net.Listener
	handler  http.Handler
	address  string
}

// Start start the http server
func (s *HTTPServer) Serve() error {
	server := &http.Server{Handler: &delegateHandler{
		handler:      s.handler,
		proxyAddress: s.address,
	}}
	return server.Serve(s.listener)
}

type delegateHandler struct {
	handler      http.Handler
	proxyAddress string
}

func (h *delegateHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	var uri = r.RequestURI
	if uri == "/proxy.pac" {
		logger.Debug("request pac from user-agent: {}", r.UserAgent())
		// for netdisk hosts, they are block at my company, need use this proxy by direct connector
		bytes, err := json.Marshal(setting.netDiskHosts)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Header().Set("Content-Type", "text/plain; charset=UTF-8")
			w.Write([]byte(err.Error()))
			return
		}
		var pac = `
var proxy = '` + h.proxyAddress + `';
var rules = ` + string(bytes) + `;

function FindProxyForURL(url, host) {
    for (var i = 0; i < rules.length; i++) {
        var rule = rules[i];
        if (rule == host ||  shExpMatch(host, '*.' + rule)) {
			return 'SOCKS ' + proxy + '; PROXY ' + proxy;
		}
    }
    return 'DIRECT';
}
`
		w.Header().Set("Content-Type", "application/x-ns-proxy-autoconfig")
		w.Write([]byte(pac))
		return
	}

	if h.handler == nil {
		http.NotFound(w, r)
		return
	}

	h.handler.ServeHTTP(w, r)
}
