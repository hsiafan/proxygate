package base

import (
	"bufio"
	"encoding/base64"
	"errors"
	"net"
	"net/http"
	"net/url"
	"strconv"

	"fmt"
	"github.com/shadowsocks/shadowsocks-go/shadowsocks"
	"golang.org/x/net/proxy"
)

// Connector connect to target address via proxy (or direct)
type Connector interface {
	fmt.Stringer
	// Create new connection
	DialTCP(address string) (conn net.Conn, err error)
}

var _ Connector = (*DirectConnector)(nil)

// DirectConnector do use dial directly
type DirectConnector struct {
	// create direct connection
	dialer net.Dialer
}

// Name the DirectConnector
func (*DirectConnector) String() string {
	return "direct"
}

// NewDirectConnector create new DirectConnector
func NewDirectConnector() *DirectConnector {
	return &DirectConnector{
		dialer: net.Dialer{Timeout: setting.dialTimeout},
	}
}

// DialTCP create connection directly
func (dc *DirectConnector) DialTCP(address string) (conn net.Conn, err error) {
	logger.Debug("create direct connection for {}", address)

	address = resolveAddress(address)

	conn, err = dc.dialer.Dial("tcp", address)
	if err != nil {
		logger.Warn("direct dial to {} failed", address)
	}
	return
}

// try to do dns resolve for domain name, only use ipv4.
// this is because some domain(weiyun, etc) occur timeout for ipv6 name resolve.
// TODO: remove this if weiyun fix ipv6 dns problem or go provide way to prefer ipv4
func resolveAddress(address string) string {
	host, port, err := net.SplitHostPort(address)
	if err != nil { // should not happen
		return address
	}
	if GetHostType(host) != DomainHost {
		return address
	}
	addrs, _, err := cgoLookupIPCNAME(host)
	if err != nil {
		return address
	}
	return net.JoinHostPort(addrs[0].String(), port)
}

// ShadowSocksConnector dial via shadow-socks server
type ShadowSocksConnector struct {
	server string
	cipher *shadowsocks.Cipher
}

var _ Connector = (*ShadowSocksConnector)(nil)

// NewShadowSocksConnector init shadow socks connector
func NewShadowSocksConnector(server *SecondaryProxy) (*ShadowSocksConnector, error) {
	method := server.User // using user to store ss method
	// only one encryption table
	cipher, err := shadowsocks.NewCipher(method, server.Password)
	if err != nil {
		logger.Error("failed generating ciphers:", err)
		return nil, err
	}
	serverAddress := server.Address()

	return &ShadowSocksConnector{server: serverAddress, cipher: cipher}, nil
}

// Name return shadow-socks info
func (ssc *ShadowSocksConnector) String() string {
	return "ShadowSocks://" + ssc.server
}

// DialTCP connect via shadow socks
func (ssc *ShadowSocksConnector) DialTCP(address string) (net.Conn, error) {
	logger.Debug("create shadow socks connection for {} by {}", address, ssc.server)
	conn, err := shadowsocks.Dial(address, ssc.server, ssc.cipher.Copy())
	if err != nil {
		logger.Warn("dial via {} to {} failed", ssc, address)
		return nil, err
	}
	return conn, nil
}

// SocksConnector connector create connection, via socks5 proxy
type SocksConnector struct {
	address string
	auth    *proxy.Auth
	dialer  proxy.Dialer
}

var _ Connector = (*SocksConnector)(nil)

// NewSocksProxyConnector init socks5 proxy connector
func NewSocksProxyConnector(server *SecondaryProxy) (*SocksConnector, error) {
	address := server.Address()
	var auth *proxy.Auth
	if len(server.User) > 0 {
		auth = &proxy.Auth{User: server.User, Password: server.Password}
	}
	dialer := net.Dialer{Timeout: setting.dialTimeout}
	socksDialer, err := proxy.SOCKS5("tcp", address, auth, &dialer)
	if err != nil {
		return nil, err
	}
	return &SocksConnector{
		address: address,
		auth:    auth,
		dialer:  socksDialer,
	}, nil
}

// Name return socks proxy name
func (sc *SocksConnector) String() string {
	return "socks5://" + sc.address
}

// DialTCP connect via socks proxy
func (sc *SocksConnector) DialTCP(address string) (conn net.Conn, err error) {
	logger.Debug("create socks proxy connection for", address)
	conn, err = sc.dialer.Dial("tcp", address)
	if err != nil {
		logger.Warn("dial via {} to {} failed", sc, address)
	}
	return
}

var _ Connector = (*HTTPConnector)(nil)

// HTTPConnector create connection, via http proxy tunnel.
// This require http proxy support connect method
type HTTPConnector struct {
	address string
	auth    *proxy.Auth
	dialer  net.Dialer
}

// Name return http proxy identity and address
func (hc *HTTPConnector) String() string {
	return "http://" + hc.address
}

// NewHTTPConnector init http proxy connector
func NewHTTPConnector(server *SecondaryProxy) (*HTTPConnector, error) {
	address := server.Address()
	var auth *proxy.Auth
	if len(server.User) > 0 {
		auth = &proxy.Auth{User: server.User, Password: server.Password}
	}
	return &HTTPConnector{
		address: address,
		auth:    auth,
		dialer:  net.Dialer{Timeout: setting.dialTimeout},
	}, nil
}

// DialTCP connect via http proxy tunnel
func (hc *HTTPConnector) DialTCP(address string) (net.Conn, error) {
	logger.Debug("create connection via http proxy for", address)
	conn, err := hc.dialer.Dial("tcp", hc.address)
	if err != nil {
		return nil, err
	}
	req := &http.Request{
		Method:     http.MethodConnect,
		URL:        &url.URL{Host: address},
		Host:       address,
		ProtoMajor: 1,
		ProtoMinor: 1,
		Header:     make(http.Header),
	}
	req.Header.Set("Proxy-Connection", "keep-alive")
	req.Header.Set("Connection", "keep-alive")
	if hc.auth != nil {
		user := hc.auth.User
		password := hc.auth.Password
		auth := "Basic " + base64.StdEncoding.EncodeToString([]byte(user+":"+password))
		req.Header.Set("Proxy-Authorization", auth)
	}
	if err := req.Write(conn); err != nil {
		logger.Warn("dial via {} to {} failed", hc, address)
		return nil, err
	}

	resp, err := http.ReadResponse(bufio.NewReader(conn), req)
	if err != nil {
		logger.Warn("dial via {} to {} failed", hc, address)
		return nil, err
	}
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("http proxy return status: " + strconv.Itoa(resp.StatusCode))
	}
	return conn, nil
}
