// this package contains implementation for proxies
package base

import (
	"github.com/clearthesky/vlog"
	"github.com/soheilhy/cmux"
	"io"
	"bytes"
	"net"
	"sync"
	"strconv"
	"errors"
	"net/http"
)

var logger = vlog.CurrentPackageLogger()

// Server the proxy server
type Server struct {
	listener    net.Listener
	proxyRouter *ProxyRouter
	lock        sync.Mutex
	config      *Config
	HttpHandler http.Handler // handler for plain http request
	running     bool
}

// NewServer create new ProxyServer
func NewServer() (*Server, error) {
	config, err := loadConfig()
	if err != nil {
		return nil, err
	}
	return &Server{
		proxyRouter: NewProxyRouter(),
		config:      config,
	}, nil
}

// Running return if server is running now
func (s *Server) Running() bool {
	s.lock.Lock()
	var running = s.running
	s.lock.Unlock()
	return running
}

// setRunning update running
func (s *Server) setRunning(running bool) {
	s.lock.Lock()
	s.running = running
	s.lock.Unlock()
}

// Servers return the proxy servers current use
func (s *Server) Proxies() []*SecondaryProxy {
	s.lock.Lock()
	defer s.lock.Unlock()
	return copyProxies(s.config.Proxies)
}

// LocalSetting return local proxy setting
func (s *Server) LocalSetting() *LocalSetting {
	s.lock.Lock()
	defer s.lock.Unlock()
	return copyLocalSetting(s.config.LocalSetting)
}

// SetLocalSetting set new local setting. Note that you need to restart server to make new setting take effect
func (s *Server) SetLocalSetting(setting *LocalSetting) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	s.config.LocalSetting = copyLocalSetting(setting)
	if err := saveConfig(s.config); err != nil {
		return err
	}
	return nil
}

// AddProxy add one new proxy, and return the proxy's id
func (s *Server) AddProxy(proxy *SecondaryProxy) (int, error) {
	proxy = copyProxy(proxy)
	s.lock.Lock()
	defer s.lock.Unlock()
	var id = 0
	for _, p := range s.config.Proxies {
		if id < p.ID {
			id = p.ID
		}
	}
	proxy.ID = id + 1

	s.config.Proxies = append(s.config.Proxies, proxy)
	s.disableOtherProxies(proxy)
	if err := saveConfig(s.config); err != nil {
		return -1, err
	}
	s.proxyRouter.SetProxies(s.config.Proxies)
	return proxy.ID, nil
}

// UpdateProxy update proxy
func (s *Server) UpdateProxy(proxy *SecondaryProxy) error {
	proxy = copyProxy(proxy)
	s.lock.Lock()
	defer s.lock.Unlock()
	for idx, p := range s.config.Proxies {
		if p.ID == proxy.ID {
			s.config.Proxies[idx] = proxy
			s.disableOtherProxies(proxy)
			if err := saveConfig(s.config); err != nil {
				return err
			}
			s.proxyRouter.SetProxies(s.config.Proxies)
			return nil
		}
	}
	return errors.New("proxy not found")
}

func (s *Server) disableOtherProxies(proxy *SecondaryProxy) {
	if !proxy.Enable {
		return
	}
	for _, p := range s.config.Proxies {
		if proxy.ID != p.ID && p.Enable {
			p.Enable = false
		}
	}
}

// DeleteProxy delete a proxy
func (s *Server) DeleteProxy(id int) error {
	s.lock.Lock()
	defer s.lock.Unlock()
	var found = false
	var newProxies []*SecondaryProxy
	for _, server := range s.config.Proxies {
		if server.ID == id {
			found = true
		} else {
			newProxies = append(newProxies, server)
		}
	}
	if !found {
		return errors.New("Server not found, id: " + strconv.Itoa(id))
	}
	s.config.Proxies = newProxies
	if err := saveConfig(s.config); err != nil {
		return err
	}
	s.proxyRouter.SetProxies(newProxies)
	return nil
}

// Start server
func (s *Server) Start() error {
	s.lock.Lock()
	defer s.lock.Unlock()

	var c = s.config
	var proxies = c.Proxies
	var setting = c.LocalSetting
	s.proxyRouter.SetProxies(proxies)

	var address = net.JoinHostPort(setting.Listen, strconv.Itoa(setting.Port))
	logger.Info("start server at {}", address)
	listener, err := net.Listen("tcp", address)
	if err != nil {
		logger.Critical("listen on address err: {}", address, err)
		return err
	}
	s.listener = listener

	mux := cmux.New(listener)
	socks5Listener := mux.Match(socks5ProxyMatcher)
	httpProxyListener := mux.Match(httpProxyMatcher)
	var httpServerListener = mux.Match(cmux.HTTP1Fast())
	tcpListener := mux.Match(cmux.Any())

	var httpServer = &HTTPServer{
		listener: httpServerListener,
		handler:  s.HttpHandler,
		address:  address,
	}
	go httpServer.Serve()
	httpProxy := NewHTTPProxy(httpProxyListener, setting.Timeout, s.proxyRouter)
	go httpProxy.Serve()
	socks5Proxy := NewSocks5Proxy(socks5Listener, setting.Timeout, s.proxyRouter)
	go socks5Proxy.Serve()

	go func() {
		for {
			conn, err := tcpListener.Accept()
			if err != nil {
				break
			}
			// just close the connection
			logger.Warn("unexpected connection received from {}, close it", conn.RemoteAddr())
			conn.Close()
		}
	}()

	go func() {
		err := mux.Serve()
		if err != nil {
			logger.Debug("server error:", err)
		}
	}()
	s.running = true
	return nil
}

// Stop stop the server
func (s *Server) Stop() {
	if s.listener == nil {
		return
	}
	s.lock.Lock()
	defer s.lock.Unlock()
	logger.Info("stop server...")
	s.listener.Close()
	s.listener = nil
	s.running = false
}

var httpMethods = map[string]bool{
	"OPTIONS": true,
	"GET":     true,
	"HEAD":    true,
	"POST":    true,
	"PUT":     true,
	"DELETE":  true,
	"TRACE":   true,
	"CONNECT": true,
}

// http proxy matcher only check method and uri prefix
func httpProxyMatcher(r io.Reader) bool {
	var buf = make([]byte, 10)
	if _, err := io.ReadAtLeast(r, buf, 10); err != nil {
		return false
	}
	idx := bytes.IndexByte(buf, ' ')
	if idx < 0 {
		return false
	}
	method := string(buf[:idx])
	if !httpMethods[method] {
		return false
	}
	if buf[idx+1] == '/' {
		// plain http request
		return false
	}
	return true
}

// socks5 proxy matcher only check first two bytes, to see if is a socks5 proxy connection.
func socks5ProxyMatcher(r io.Reader) bool {
	var buf = make([]byte, 2)
	if _, err := io.ReadAtLeast(r, buf, 2); err != nil {
		return false
	}

	return buf[0] == 5
}

func copyProxy(proxy *SecondaryProxy) *SecondaryProxy {
	var p = new(SecondaryProxy)
	*p = *proxy
	return p
}

func copyProxies(proxies []*SecondaryProxy) []*SecondaryProxy {
	var newProxies = make([]*SecondaryProxy, len(proxies))
	for idx := range proxies {
		var proxy = new(SecondaryProxy)
		*proxy = *proxies[idx]
		newProxies[idx] = proxy
	}
	return newProxies
}

func copyLocalSetting(s *LocalSetting) *LocalSetting {
	var n = new(LocalSetting)
	*n = *s
	return n
}
