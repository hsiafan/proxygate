package base

import (
	"net"
	"time"
)

var zeroTime = time.Time{}

var _ net.Conn = (*IdleTimeoutConnWrapper)(nil)

// IdleTimeoutConnWrapper wrap connection, with idle timeout. when idle timeout is reach, close the connection.
type IdleTimeoutConnWrapper struct {
	net.Conn
	timer   *time.Timer
	timeout time.Duration // the idle timeout duration
}

func (tc *IdleTimeoutConnWrapper) Read(b []byte) (n int, err error) {
	tc.timer.Reset(tc.timeout)
	n, err = tc.Conn.Read(b)
	return
}

func (tc *IdleTimeoutConnWrapper) Write(b []byte) (n int, err error) {
	tc.timer.Reset(tc.timeout)
	n, err = tc.Conn.Write(b)
	return
}

func (tc *IdleTimeoutConnWrapper) Close() error {
	tc.timer.Stop()
	return tc.Conn.Close()
}

// NewTimeoutConn create new timeoutConn
func NewTimeoutConn(conn net.Conn, timeout time.Duration) net.Conn {
	conn.SetWriteDeadline(zeroTime)
	return &IdleTimeoutConnWrapper{
		Conn:    conn,
		timeout: timeout,
		timer: time.AfterFunc(timeout, func() {
			logger.Debug("conn with {} idle timeout, close", conn.RemoteAddr())
			conn.Close()
		}),
	}
}
