package base

import (
	"net"
	"time"
)

type DomainType int

const (
	Ipv4Host   DomainType = 1
	Ipv6Host   DomainType = 2
	DomainHost DomainType = 3
)

// GetHostType see host is domain, ipv4, or ipv6
func GetHostType(host string) DomainType {
	var ip = net.ParseIP(host)
	if ip == nil {
		return DomainHost
	}
	if ip.To4() != nil {
		return Ipv4Host
	}
	return Ipv6Host
}

type ProxyStatus int

const (
	ProxyOK      ProxyStatus = 0
	ProxyTimeout ProxyStatus = 1
	ProxyErr     ProxyStatus = 2
)

// CheckProxyServer check availability for one proxy
func CheckProxyServer(proxy *SecondaryProxy) (ProxyStatus, time.Duration) {
	var start = time.Now()
	var conn, err = net.DialTimeout("tcp", proxy.Address(), setting.dialTimeout)
	if err != nil {
		if err, ok := err.(net.Error); ok && err.Timeout() {
			return ProxyTimeout, time.Now().Sub(start)
		}
		return ProxyErr, time.Now().Sub(start)
	}
	conn.Close()
	return ProxyOK, time.Now().Sub(start)
}
