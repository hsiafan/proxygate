package base

import (
	"encoding/binary"
	"errors"
	"io"
	"net"
	"runtime/debug"
	"strconv"
	"time"
)

const (
	ipv4Addr   = 1
	domainAddr = 3
	ipv6Addr   = 4
)

// socks response code
const (
	succeeded          = 0
	socksServerError   = 1
	commandNotSupport  = 7
	addrTypeNotSupport = 8
)

// Socks5Proxy socks5 proxy server only support connect command
type Socks5Proxy struct {
	listener  net.Listener
	connector Connector
	timeout   time.Duration
}

// NewSocks5Proxy create new socks proxy
func NewSocks5Proxy(listener net.Listener, timeout int, connector Connector) *Socks5Proxy {
	return &Socks5Proxy{listener: listener, connector: connector, timeout: time.Duration(timeout) * time.Second}
}

// Start the socks proxy
func (proxy *Socks5Proxy) Serve() error {
	for {
		conn, err := proxy.listener.Accept()
		if err != nil {
			logger.Debug("socks proxy closed")
			break
		}
		go proxy.handleConnection(conn)
	}
	return nil
}

// handle socks connection
func (proxy *Socks5Proxy) handleConnection(conn net.Conn) {
	defer func() {
		if err := recover(); err != nil {
			logger.Error("socks5 proxy worker panic:{}\n{}", err, string(debug.Stack()))
		}
	}()

	conn = NewTimeoutConn(conn, proxy.timeout)
	defer conn.Close()

	if err := handShake(conn); err != nil && err != io.EOF {
		logger.Error("socks5 handshake error:", err)
		return
	}

	address, err := readConnectMessage(conn)
	logger.Debug("socks5 proxy connect to", address)
	if err != nil {
		if err != io.EOF {
			logger.Warn("error getting request:", err)
		}
		return
	}

	remoteConn, err := proxy.connector.DialTCP(address)
	if err != nil {
		logger.Debug("socks5 proxy failed connect to {} use {}, error: {} ", address, proxy.connector, err)
		conn.Write([]byte{0x05, socksServerError, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00})
		return
	}
	remoteConn = NewTimeoutConn(remoteConn, proxy.timeout)
	defer remoteConn.Close()

	// sending connection established message immediately to client.
	var remoteAddress = remoteConn.RemoteAddr().String()
	remoteHost, remotePortStr, err := net.SplitHostPort(remoteAddress)
	if err != nil {
		// should not happen
		remoteHost = "0.0.0.0"
	}
	var ip = net.ParseIP(remoteHost)
	var response = []byte{0x05, succeeded, 0x00}
	if ip == nil {
		response = append(response, domainAddr)
		response = append(response, []byte(remoteHost)...)
	} else {
		var ipv4 = ip.To4()
		if ipv4 != nil {
			response = append(response, ipv4Addr)
			response = append(response, ipv4...)
		} else {
			var ipv6 = ip.To16()
			response = append(response, ipv6Addr)
			response = append(response, ipv6...)
		}
	}

	port, err := strconv.Atoi(remotePortStr)
	if err != nil {
		port = 4444
	}
	response = append(response, 0, 0)
	binary.BigEndian.PutUint16(response[len(response)-2:], uint16(port))

	_, err = conn.Write(response)
	if err != nil {
		logger.Warn("socks5 proxy write response error, error: {} ", address, err)
		return
	}

	proxyTraffics(remoteConn, conn)

}

// hand shake with socks client
func handShake(conn net.Conn) (error) {
	var buf = make([]byte, 256)

	// read socks version and n-method
	if _, err := io.ReadFull(conn, buf[0:2]); err != nil {
		return err
	}

	var version = int(buf[0])
	var methodNum = int(buf[1])
	if version != 5 {
		return errors.New("socks version not supported:" + strconv.Itoa(version))
	}

	// read methods
	if _, err := io.ReadFull(conn, buf[0:methodNum]); err != nil {
		return err
	}
	if _, err := conn.Write([]byte{5, 0}); err != nil {
		return err
	}
	return nil
}

// handle socks5 connect request
func readConnectMessage(conn net.Conn) (string, error) {
	var buf = make([]byte, 256) // domain may have up tp 255 bytes

	// read socks version and command
	if _, err := io.ReadFull(conn, buf[0:2]); err != nil {
		return "", err
	}
	var version = int(buf[0])
	var command = int(buf[1])

	// check version and cmd
	if version != 5 {
		return "", errors.New("socks version not supported: " + strconv.Itoa(version))
	}
	if command != 1 { // not connect command
		if _, err := conn.Write([]byte{0x05, commandNotSupport, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}); err != nil {
			logger.Warn("socks5 proxy write response error, error: {} ", err)
		}
		return "", errors.New("socks command not supported: " + strconv.Itoa(command))
	}

	// read address type
	if _, err := io.ReadFull(conn, buf[0:2]); err != nil {
		return "", err
	}

	var addressType = int(buf[1])
	var host string
	switch addressType {
	case ipv4Addr:
		if _, err := io.ReadFull(conn, buf[0:net.IPv4len]); err != nil {
			return "", err
		}
		host = net.IP(buf[0:net.IPv4len]).String()
	case domainAddr:
		if _, err := io.ReadFull(conn, buf[0:1]); err != nil {
			return "", err
		}
		var domainLen = int(buf[0])
		if _, err := io.ReadFull(conn, buf[0:domainLen]); err != nil {
			return "", err
		}
		host = string(buf[0:domainLen])
	case ipv6Addr:
		if _, err := io.ReadFull(conn, buf[0:net.IPv6len]); err != nil {
			return "", err
		}
		host = net.IP(buf[0:net.IPv6len]).String()
	default:
		if _, err := conn.Write([]byte{0x05, addrTypeNotSupport, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}); err != nil {
			logger.Warn("socks5 proxy write response error, error: {} ", err)
		}
		return "", errors.New("unsupported address type :" + strconv.Itoa(addressType))
	}

	// read port
	if _, err := io.ReadFull(conn, buf[0:2]); err != nil {
		return "", err
	}
	var port = int(binary.BigEndian.Uint16(buf[0:2]))
	return net.JoinHostPort(host, strconv.Itoa(port)), nil
}
