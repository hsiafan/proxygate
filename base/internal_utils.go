package base

import (
	"github.com/shadowsocks/shadowsocks-go/shadowsocks"
	"github.com/soheilhy/cmux"
	"io"
	"net"
	"reflect"
	"strings"
	"sync"
)

// proxy traffics
func proxyTraffics(dst net.Conn, src net.Conn) {
	defer dst.Close()
	defer src.Close()

	wg := &sync.WaitGroup{}
	wg.Add(2)
	go func() {
		bypassFirewall(dst, src)
		pipeStream(dst, src, wg)
	}()
	pipeStream(src, dst, wg)
	wg.Wait()
}

// this is for bypass the firewall in my company which block all net disk http requests
// TODO: only do this for special domains, and when do not use proxy server?
func bypassFirewall(dst net.Conn, src net.Conn) {
	var buf = make([]byte, 5)
	read, err := src.Read(buf)
	if err != nil {
		logger.Debug("read from src error: {}", err)
		return
	}
	written, err := dst.Write(buf[:read])
	if err != nil {
		logger.Warn("write to dst error: {}", err)
		return
	}
	if written != read {
		logger.Error("copy from src to dst error:", io.ErrShortWrite)
		return
	}
}

func pipeStream(dst net.Conn, src net.Conn, wg *sync.WaitGroup) {
	defer wg.Done()
	defer tryCloseWrite(dst)
	defer tryCloseRead(src)
	buf := make([]byte, 4*1024)
	for {
		nr, readError := src.Read(buf)
		if nr > 0 {
			nw, writeError := dst.Write(buf[0:nr])
			if writeError != nil {
				logger.Debug("write to dst conn error:", writeError)
				break
			}
			if nr != nw {
				logger.Error("copy from src to dst error:", io.ErrShortWrite)
				break
			}
		}
		if readError == io.EOF {
			break
		}
		if readError != nil {
			if strings.HasSuffix(readError.Error(), "use of closed network connection") {
				// due to incorrect close the connection(as shadowSocks connection do). just ignore it now
				break
			}
			logger.Debug("read from src conn error:", readError)
			break
		}
	}
}

func tryCloseWrite(conn net.Conn) {
	switch conn.(type) {
	case *IdleTimeoutConnWrapper:
		tryCloseWrite(conn.(*IdleTimeoutConnWrapper).Conn)
	case *net.TCPConn:
		conn.(*net.TCPConn).CloseWrite()
	case *shadowsocks.Conn:
		//TODO: shadow socks do not provide way to half close a connection
		conn.Close()
	case *cmux.MuxConn:
		tryCloseWrite(conn.(*cmux.MuxConn).Conn)
	default:
		logger.Warn("conn type:", reflect.TypeOf(conn), "not well handled")
		conn.Close()
	}
}

func tryCloseRead(conn net.Conn) {
	switch conn.(type) {
	case *IdleTimeoutConnWrapper:
		tryCloseRead(conn.(*IdleTimeoutConnWrapper).Conn)
	case *net.TCPConn:
		conn.(*net.TCPConn).CloseRead()
	case *shadowsocks.Conn:
		conn.Close()
	case *cmux.MuxConn:
		tryCloseRead(conn.(*cmux.MuxConn).Conn)
	default:
		logger.Warn("conn type:", reflect.TypeOf(conn), "not well handled")
		conn.Close()
	}
}
