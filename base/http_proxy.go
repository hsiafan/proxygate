package base

import (
	"errors"
	"io"
	"net"
	"net/http"
	"runtime/debug"
	"time"
)

var _ http.Handler = (*HTTPProxy)(nil)

// HTTPProxy The http proxy implements http.Handler.
type HTTPProxy struct {
	listener  net.Listener
	connector Connector
	dial      func(network, address string) (net.Conn, error)
	transport *http.Transport
	timeout   time.Duration
	server    *http.Server
}

// NewHTTPProxy create new Http Proxy
func NewHTTPProxy(listener net.Listener, timeout int, connector Connector) *HTTPProxy {
	dial := func(network, address string) (net.Conn, error) {
		if network != "tcp" {
			return nil, errors.New("unsupported network: " + network)
		}
		return connector.DialTCP(address)
	}
	return &HTTPProxy{
		listener:  listener,
		connector: connector,
		dial:      dial,
		transport: &http.Transport{Dial: dial},
		timeout:   time.Duration(timeout) * time.Second,
	}
}

// Start start the http proxy
func (proxy *HTTPProxy) Serve() error {
	server := &http.Server{Handler: proxy}
	return server.Serve(proxy.listener)
}

// Standard net/http function. Shouldn't be used directly, http.Serve will use it.
func (proxy *HTTPProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if err := recover(); err != nil {
			logger.Error("http proxy worker panic:{}\n{}", err, string(debug.Stack()))
		}
	}()
	//r.Header["X-Forwarded-For"] = w.RemoteAddr()
	if r.Method == "CONNECT" {
		proxy.proxyConnect(w, r)
	} else {
		proxy.proxyRequest(w, r)
	}
}

func (proxy *HTTPProxy) proxyConnect(w http.ResponseWriter, r *http.Request) {
	hj, _ := w.(http.Hijacker)
	clientConn, _, err := hj.Hijack()
	if err != nil {
		logger.Debug("failed to get tcp connection of client:", r.RequestURI)
		clientConn.Write([]byte("HTTP/1.1 400 Bad Request\r\n\r\n"))
		return
	}
	clientConn = NewTimeoutConn(clientConn, proxy.timeout)
	defer clientConn.Close()

	address := r.URL.Host
	logger.Debug("http proxy connect to", address)
	remoteConn, err := proxy.dial("tcp", address)
	if err != nil {
		logger.Debug("http connect to {} use {} error: {}", address, proxy.connector, err)
		clientConn.Write([]byte("HTTP/1.1 502 Bad Gateway\r\n\r\n"))
		return
	}
	remoteConn = NewTimeoutConn(remoteConn, proxy.timeout)
	defer remoteConn.Close()

	clientConn.Write([]byte("HTTP/1.1 200 Connection Established\r\n\r\n"))

	proxyTraffics(remoteConn, clientConn)
}

func (proxy *HTTPProxy) proxyRequest(w http.ResponseWriter, r *http.Request) {
	logger.Debug("http proxy, url:", r.RequestURI)
	removeProxyHeaders(r)
	resp, err := proxy.transport.RoundTrip(r)
	if err == io.EOF {
		logger.Debug("unexpected closed connection")
		return
	} else if err != nil {
		logger.Debug("fetch response error use {} error: {}", proxy.connector, err)
		w.WriteHeader(502)
		w.Write([]byte("fetch response error"))
		return
	}
	defer resp.Body.Close()

	for k := range w.Header() {
		w.Header().Del(k)
	}
	// CopyHeaders copy headers from source to destination.
	for key, values := range resp.Header {
		for _, value := range values {
			w.Header().Add(key, value)
		}
	}
	w.WriteHeader(resp.StatusCode)
	_, err = io.Copy(w, resp.Body)
	if err != nil && err != io.EOF {
		logger.Error("got an error when copy remote response to client:", err)
		//TODO: we should close client connection?
		return
	}
}

func removeProxyHeaders(r *http.Request) {
	r.RequestURI = ""
	r.Header.Del("Accept-Encoding")
	r.Header.Del("Proxy-Connection")
	r.Header.Del("Proxy-Authenticate")
	r.Header.Del("Proxy-Authorization")
	r.Header.Del("Connection")
}
