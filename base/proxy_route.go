package base

import (
	"net"
	"sync"
	"errors"
	"strings"
)

// determine which proxy should be used

// for type check
var _ Connector = (*ProxyRouter)(nil)

type proxyEntry struct {
	proxy     *SecondaryProxy
	connector Connector
}

func (pe *proxyEntry) enable() bool {
	return pe.proxy.Enable && pe.connector != nil
}

// ProxyRouter select one connector. now just use the first connector...
type ProxyRouter struct {
	entries         []proxyEntry
	lock            sync.RWMutex
	directConnector Connector
}

// NewProxyRouter create new route connector
func NewProxyRouter() *ProxyRouter {
	c := &ProxyRouter{
		directConnector: NewDirectConnector(),
	}
	return c
}

// SetProxies update proxies
func (r *ProxyRouter) SetProxies(proxies []*SecondaryProxy) {
	entries := make([]proxyEntry, len(proxies))
	for idx, proxy := range proxies {
		entries[idx].proxy = proxy
		c, err := r.createProxyConnector(proxy)
		if err != nil {
			logger.Warn("create connector for {} proxy {}:{} failed:", proxy.Type, proxy.Host, proxy.Port, err)
			continue
		}
		entries[idx].connector = c
	}

	r.lock.Lock()
	r.entries = entries
	r.lock.Unlock()
}

// create connector using the given proxy server
func (r *ProxyRouter) createProxyConnector(server *SecondaryProxy) (Connector, error) {
	if server.Type == SHADOW_SOCKS {
		return NewShadowSocksConnector(server)
	}

	if server.Type == SOCKS {
		return NewSocksProxyConnector(server)
	}
	if server.Type == HTTP {
		return NewHTTPConnector(server)
	}
	// nothing
	return nil, errors.New("unknown server type: " + string(server.Type))
}

// Name return ProxyRouter
func (r *ProxyRouter) String() string {
	r.lock.RLock()
	defer r.lock.RUnlock()
	return "ConnectorRouter"
}

// DialTCP create connection
func (r *ProxyRouter) DialTCP(address string) (net.Conn, error) {
	host, _, err := net.SplitHostPort(address)
	if err != nil {
		return nil, errors.New("address not valid: " + err.Error())
	}

	var netDiskHost = false
	for _, nh := range setting.netDiskHosts {
		if matchHost(host, nh) {
			netDiskHost = true
			break
		}
	}
	var connector Connector
	if netDiskHost {
		connector = r.directConnector
	} else {
		r.lock.RLock()
		connector = r.selected()
		r.lock.RUnlock()
	}

	logger.Debug("dial to {} use {}", address, connector)
	conn, err := connector.DialTCP(address)
	if err != nil {
		logger.Debug("dial to {} use {} failed: {}", address, err)
	}
	return conn, err
}

func (r *ProxyRouter) selected() Connector {
	for _, entry := range r.entries {
		if entry.enable() {
			return entry.connector
		}
	}
	return r.directConnector
}

// Match returns if this rule match host
func matchHost(host string, suffix string) bool {
	if suffix == host || len(host) > len(suffix) && strings.HasSuffix(host, suffix) &&
		host[len(host)-len(suffix)-1] == '.' {
		return true
	}
	return false
}
