// +build cgo
// +build darwin dragonfly freebsd linux netbsd openbsd solaris
package base

/*
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
*/
import "C"

import (
	"syscall"
	"unsafe"
	"net"
	"errors"
)

type addrinfoErrno int

func (eai addrinfoErrno) Error() string   { return C.GoString(C.gai_strerror(C.int(eai))) }
func (eai addrinfoErrno) Temporary() bool { return eai == C.EAI_AGAIN }
func (eai addrinfoErrno) Timeout() bool   { return false }

func cgoLookupIPCNAME(name string) (addrs []net.IPAddr, cname string, err error) {

	var hints C.struct_addrinfo
	hints.ai_family = C.AF_INET
	hints.ai_flags = C.AI_CANONNAME
	hints.ai_socktype = C.SOCK_STREAM

	host := C.CString(name)
	defer C.free(unsafe.Pointer(host))
	var result *C.struct_addrinfo
	gerrno, err := C.getaddrinfo(host, nil, &hints, &result)
	if gerrno != 0 {
		switch gerrno {
		case C.EAI_SYSTEM:
			if err == nil {
				// err should not be nil, but sometimes getaddrinfo returns
				// gerrno == C.EAI_SYSTEM with err == nil on Linux.
				// The report claims that it happens when we have too many
				// open files, so use syscall.EMFILE (too many open files in system).
				// Most system calls would return ENFILE (too many open files),
				// so at the least EMFILE should be easy to recognize if this
				// comes up again. golang.org/issue/6232.
				err = syscall.EMFILE
			}
		case C.EAI_NONAME:
			err = errors.New("no such host")
		default:
			err = addrinfoErrno(gerrno)
		}
		return nil, "", &net.DNSError{Err: err.Error(), Name: name}
	}
	defer C.freeaddrinfo(result)

	if result != nil {
		cname = C.GoString(result.ai_canonname)
		if cname == "" {
			cname = name
		}
		if len(cname) > 0 && cname[len(cname)-1] != '.' {
			cname += "."
		}
	}
	for r := result; r != nil; r = r.ai_next {
		// We only asked for SOCK_STREAM, but check anyhow.
		if r.ai_socktype != C.SOCK_STREAM {
			continue
		}
		switch r.ai_family {
		case C.AF_INET:
			sa := (*syscall.RawSockaddrInet4)(unsafe.Pointer(r.ai_addr))
			addr := net.IPAddr{IP: copyIP(sa.Addr[:])}
			addrs = append(addrs, addr)
		case C.AF_INET6:
			continue
		}
	}
	return addrs, cname, nil
}

func copyIP(x net.IP) net.IP {
	if len(x) < 16 {
		return x.To16()
	}
	y := make(net.IP, len(x))
	copy(y, x)
	return y
}
