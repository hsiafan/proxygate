package base

import (
	"encoding/json"
	"io/ioutil"
	"net"
	"os"
	"os/user"
	"path/filepath"
	"strconv"
	"time"
)

const (
	SHADOW_SOCKS string = "shadowsocks"
	SOCKS        string = "socks"
	HTTP         string = "http"
)

var ProxyTypes = []string{SHADOW_SOCKS, SOCKS, HTTP}

var ShadowSocksMethods = []string{"aes-128-cfb", "aes-192-cfb", "aes-256-cfb", "bf-cfb", "cast5-cfb", "des-cfb",
	"rc4-md5", "chacha20", "salsa20", "rc4", "table"}

var setting = struct {
	configDirectory string
	configPath      string
	switchRulePath  string
	dialTimeout     time.Duration // tcp dial timeout
	netDiskHosts    []string
}{
	configDirectory: configDirectory(),
	configPath:      filepath.Join(configDirectory(), "config.json"),
	switchRulePath:  filepath.Join(configDirectory(), "switch_rule.txt"),
	dialTimeout:     3 * time.Second,
	netDiskHosts: []string{
		"pan.baidu.com",
		"baidupcs.com",
		"weiyun.com",
		//"ptlogin2.qq.com",
		"115.com",
		"jianguoyun.com",
		"push.apple.com",
		"icloud.com",
		"icloud-content.com",
	},
}

// Config for proxy listened
type Config struct {
	LocalSetting *LocalSetting     `json:"local"`
	Proxies      []*SecondaryProxy `json:"servers"`
}

// LocalSetting contains local proxy setting
type LocalSetting struct {
	Listen     string `json:"listen"`
	Port       int    `json:"port"`
	Timeout    int    `json:"timeout"`     // the connection idle timeout, means no read/write operation in this time.
}

// SecondaryProxy contains secondary proxy info
type SecondaryProxy struct {
	ID       int    `json:"id"`
	Type     string `json:"type"`
	Host     string `json:"host"`
	Port     int    `json:"port"`
	User     string `json:"user"`
	Password string `json:"password"`
	Enable   bool   `json:"enable"`
}

// Address join Host and Port to one single address
func (p *SecondaryProxy) Address() string {
	return net.JoinHostPort(p.Host, strconv.Itoa(p.Port))
}

// loadConfig load config file
func loadConfig() (*Config, error) {
	if _, err := os.Stat(setting.configPath); os.IsNotExist(err) {
		logger.Info("config file not exists, use default setting")
		return &Config{
			LocalSetting: &LocalSetting{
				Listen:  "localhost",
				Port:    1080,
				Timeout: 600,
			},
			Proxies: nil,
		}, nil
	}

	data, err := ioutil.ReadFile(setting.configPath)
	if err != nil {
		return nil, err
	}
	config := &Config{}
	err = json.Unmarshal(data, config)
	if err != nil {
		return nil, err
	}
	return config, nil
}

// saveConfig save config to file
func saveConfig(config *Config) error {
	data, err := json.Marshal(config)
	if err != nil {
		logger.Error("save config error:", err)
		return err
	}
	err = ioutil.WriteFile(setting.configPath, data, 0600)
	if err != nil {
		logger.Error("save config error:", err)
		return err
	}
	return nil
}

func configDirectory() string {
	currentUser, err := user.Current()
	if err != nil {
		panic(err)
	}
	dir := filepath.Join(currentUser.HomeDir, ".proxygate")
	err = os.MkdirAll(dir, 0700)
	if err != nil {
		panic(err)
	}
	return dir
}
