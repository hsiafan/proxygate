package base

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestProxySwitcherRuleMatch(t *testing.T) {
	rule := ProxySwitcherRule{
		Host:  "bbc.com",
		White: false,
	}
	assert.True(t, rule.Match("bbc.com"))
	assert.True(t, rule.Match("www.bbc.com"))
	assert.False(t, rule.Match("bbbc.com"))
}
